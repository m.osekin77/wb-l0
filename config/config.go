package config

import (
	"fmt"
	"github.com/ilyakaznacheev/cleanenv"
)

type App struct {
	Name    string `yaml:"name" env-default:"WB-L0"`
	Version string `yaml:"version" env-default:"1.0.0"`
}

type HTTP struct {
	Host string `yaml:"host" env-default:"localhost"`
	Port string `yaml:"port" env-default:"8080"`
}

type Postgres struct {
	Host     string `yaml:"host" env-default:"localhost"`
	Port     string `yaml:"port" env-default:"5432"`
	User     string `yaml:"user" env-default:"myuser"`
	Password string `yaml:"password" env-default:"mypass"`
	DBName   string `yaml:"db_name" env-default:"mydb"`
	Driver   string `yaml:"driver" env-default:"pq"`
}

type Nats struct {
	Host    string `yaml:"host"`
	Port    string `yaml:"port"`
	Cluster string `yaml:"cluster"`
	Client  string `yaml:"client"`
	Topic   string `yaml:"topic"`
}

type Config struct {
	App      App      `yaml:"app"`
	HTTP     HTTP     `yaml:"http"`
	Postgres Postgres `yaml:"postgres"`
	Nats     Nats     `yaml:"nats"`
}

func NewConfig() (config *Config, err error) {
	config = &Config{}
	if err = cleanenv.ReadConfig("./config/config.yml", config); err != nil {
		return nil, fmt.Errorf("can't create config: %#v", err)
	}
	return config, nil
}
