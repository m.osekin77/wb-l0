DO $$
    BEGIN
        IF NOT EXISTS (SELECT 1 FROM pg_database WHERE datname = 'my_db') THEN
            CREATE DATABASE my_db;
    END IF;
END $$;

\c my_db;

CREATE TABLE IF NOT EXISTS my_table
(
    order_uid          VARCHAR(255) PRIMARY KEY,
    track_number       VARCHAR(255),
    entry              VARCHAR(255),
    delivery_info      JSONB,
    payment_info       JSONB,
    items              JSONB,
    locale             VARCHAR(255),
    internal_signature VARCHAR(255),
    customer_id        VARCHAR(255),
    delivery_service   VARCHAR(255),
    shardkey           VARCHAR(255),
    sm_id              INTEGER,
    date_created       VARCHAR(255),
    oof_shard          VARCHAR(255)
);