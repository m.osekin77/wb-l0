package models

// TODO: add validation fields

type Order struct {
	OrderUid          string   `json:"order_uid" validate:"required,len=19"`
	TrackNumber       string   `json:"track_number" validate:"required,len=14"`
	Entry             string   `json:"entry" validate:"required,eq=WBIL"`
	Delivery          Delivery `json:"delivery" validate:"required"`
	Payment           Payment  `json:"payment" validate:"required"`
	Items             []Item   `json:"items" validate:"required"`
	Locale            string   `json:"locale" validate:"required,oneof=en ru"`
	InternalSignature string   `json:"internal_signature" validate:"omitempty,len=0"`
	CustomerID        string   `json:"customer_id" validate:"required"`
	DeliveryService   string   `json:"delivery_service" validate:"required"`
	ShardKey          string   `json:"shardkey" validate:"required"`
	SMID              int      `json:"sm_id" validate:"required"`
	DateCreated       string   `json:"date_created" validate:"required"`
	OOFShard          string   `json:"oof_shard" validate:"required"`
}

type Delivery struct {
	Name    string `json:"name" validate:"required"`
	Phone   string `json:"phone" validate:"required,e164"`
	Zip     string `json:"zip" validate:"required,numeric"`
	City    string `json:"city" validate:"required"`
	Address string `json:"address" validate:"required"`
	Region  string `json:"region" validate:"required"`
	Email   string `json:"email" validate:"required,email"`
}

type Payment struct {
	Transaction  string `json:"transaction" validate:"required,len=19,alphanum"`
	RequestId    string `json:"request_id" validate:"omitempty"`
	Currency     string `json:"currency" validate:"required,oneof=USD RUB EUR CNY JPY"`
	Provider     string `json:"provider" validate:"required,oneof=wbpay sberpay mirpay cnpay"`
	Amount       int    `json:"amount" validate:"required"`
	PaymentDt    int    `json:"payment_dt" validate:"required,numeric"`
	Bank         string `json:"bank" validate:"required,oneof=wb ozon tinkoff alpha rusfinance"`
	DeliveryCost int    `json:"delivery_cost" validate:"required"`
	GoodsTotal   int    `json:"goods_total" validate:"required"`
	CustomFee    int    `json:"custom_fee" validate:"omitempty"`
}

type Item struct {
	ChrtId      int    `json:"chrt_id" validate:"required,numeric"`
	TrackNumber string `json:"track_number" validate:"required,len=14"`
	Price       int    `json:"price" validate:"required,numeric"`
	Rid         string `json:"rid" validate:"required"`
	Name        string `json:"name" validate:"required,alphanum"`
	Sale        int    `json:"sale" validate:"required"`
	Size        string `json:"size" validate:"required,numeric"`
	TotalPrice  int    `json:"total_price" validate:"required,numeric"`
	NmId        int    `json:"nm_id" validate:"required"`
	Brand       string `json:"brand" validate:"required"`
	Status      int    `json:"status" validate:"required"`
}
