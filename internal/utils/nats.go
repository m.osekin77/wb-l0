package utils

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
	"l0/config"
	"l0/internal/models"
	"log"
	"time"
)

type Nats struct {
	Nc  *nats.Conn
	cfg *config.Nats
	sc  stan.Conn
}

func NewNats(cfg *config.Nats) (*Nats, error) {
	url := fmt.Sprintf("nats://%s:%s", cfg.Host, cfg.Port)
	nc, err := nats.Connect(url)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to nuts %#v", err)
	}

	log.Printf("[+] established connection to: nuts://%s:%s", cfg.Host, cfg.Port)

	sc, err := stan.Connect(cfg.Cluster, cfg.Client, stan.NatsURL(url))
	if err != nil {
		log.Println("failed to connect to stan ", err)
	}
	log.Printf("[+] established connection to stan")
	return &Nats{
		cfg: cfg,
		Nc:  nc,
		sc:  sc,
	}, nil
}

func (nats *Nats) Publish(msg *models.Order) error {
	order, err := json.Marshal(msg)
	if err != nil {
		return fmt.Errorf("failed to marshal json %#v", err)
	}
	return nats.sc.Publish(nats.cfg.Topic, order)
}

func (nats *Nats) Subscribe(ctx context.Context) (*models.Order, error) {
	var order *models.Order
	ch := make(chan *models.Order)

	_, err := nats.sc.Subscribe(nats.cfg.Topic, func(msg *stan.Msg) {
		if err := json.Unmarshal(msg.Data, &order); err != nil {
			fmt.Printf("error unmarshalling: %v", err)
		}
		ch <- order
	})

	if err != nil {
		fmt.Printf("failed to subscribe: %v", err)
	}

	select {
	case order = <-ch:
		return order, nil
	case <-ctx.Done():
		return nil, nil
	case <-time.After(5 * time.Second):
		return nil, errors.New("time out error")
	}
}
