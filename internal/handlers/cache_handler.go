package handlers

import (
	"encoding/json"
	"l0/internal/database/cache"
	"log"
	"net/http"
)

type CacheHandler struct {
	Cache *cache.Cache
}

func NewCacheHandler(cache *cache.Cache) *CacheHandler {
	return &CacheHandler{
		Cache: cache,
	}
}

func (ch *CacheHandler) GetOrderData(w http.ResponseWriter, r *http.Request) {
	uid := r.URL.Query().Get("uid")
	order, err := ch.Cache.GetOrder(uid)
	if err != nil {
		log.Println("failed to get order")
	}
	jsonData, err := json.Marshal(order)
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type")

	code, err := w.Write(jsonData)
	if err != nil {
		log.Printf("failed to send json data %#v", code)
	}
}
