package cache

import (
	"l0/internal/database/postgres"
	"l0/internal/models"
	"log"
	"sync"
)

type Cache struct {
	mu   sync.RWMutex
	data map[string]*models.Order
	db   *postgres.DB
}

func NewCache(db *postgres.DB) *Cache {
	return &Cache{
		mu:   sync.RWMutex{},
		data: make(map[string]*models.Order),
		db:   db,
	}
}

func (c *Cache) GetOrder(orderUid string) (*models.Order, error) {
	c.mu.RLock()
	defer c.mu.RUnlock()
	order, exists := c.data[orderUid]
	if !exists {
		log.Printf("order with uid %#v does not exist", orderUid)
	}

	return order, nil
}

func (c *Cache) WriteCache(order *models.Order) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.data[order.OrderUid] = order
	log.Printf("[+] order was cached id %#v\n", order.OrderUid)
}

func (c *Cache) LoadFromDB() {
	data := c.db.GetAllOrders()
	c.mu.Lock()
	defer c.mu.Unlock()
	for _, order := range data {
		c.data[order.OrderUid] = order
	}
	log.Println("[+] loaded cache from database")
}
