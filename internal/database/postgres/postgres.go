package postgres

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5/pgxpool"
	"l0/config"
	"l0/internal/models"
	"log"
)

type DB struct {
	conn *pgxpool.Pool
}

func NewDB(conn *pgxpool.Pool) *DB {
	return &DB{conn: conn}
}

func Connect(cfg *config.Postgres) (*pgxpool.Pool, error) {
	url := fmt.Sprintf("postgres://%s:%s@%s:%s/%s",
		cfg.User, cfg.Password, cfg.Host, cfg.Port, cfg.DBName)
	conn, err := pgxpool.New(context.Background(), url)

	if err != nil {
		return nil, err
	}

	return conn, nil
}

func (db *DB) InsertOrder(order *models.Order) error {
	_, err := db.conn.Exec(context.Background(),
		`INSERT INTO my_table (order_uid, track_number, entry, delivery_info, payment_info, items, locale, internal_signature, customer_id,delivery_service, shardkey, sm_id, date_created, oof_shard)
		     VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)`,
		order.OrderUid, order.TrackNumber, order.Entry, order.Delivery, order.Payment, order.Items, order.Locale, order.InternalSignature, order.CustomerID, order.DeliveryService, order.ShardKey, order.SMID, order.DateCreated, order.OOFShard)
	return err
}

func (db *DB) GetAllOrders() []*models.Order {
	var orders []*models.Order
	data, err := db.conn.Query(context.Background(), `SELECT * FROM my_table`)
	if err != nil {
		log.Println("failed to execute query")
	}

	for data.Next() {
		var order models.Order
		err = data.Scan(
			&order.OrderUid,
			&order.TrackNumber,
			&order.Entry,
			&order.Delivery,
			&order.Payment,
			&order.Items,
			&order.Locale,
			&order.InternalSignature,
			&order.CustomerID,
			&order.DeliveryService,
			&order.ShardKey,
			&order.SMID,
			&order.DateCreated,
			&order.OOFShard,
		)

		if err != nil {
			log.Println("failed to write data")
		}
		orders = append(orders, &order)
	}

	return orders
}
