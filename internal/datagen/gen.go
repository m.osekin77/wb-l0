package datagen

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"l0/internal/models"
	"log"
	rand2 "math/rand"
	"strconv"
	"strings"
	"time"
)

const (
	orderLen = 12
	trackLen = 10
	ridLen   = 17
)

const (
	customerID = "test"
	entry      = "WBIL"
)

func GenOrder() *models.Order {
	orderUID, err := genHash(orderLen)
	if err != nil {
		log.Printf("failed to create orderUID %#v", err)
	}

	trackNumber, err := genString(trackLen)

	if err != nil {
		log.Printf("failed to create track number %#v", trackNumber)
	}

	orderUID = orderUID[:5] + "mar" + orderUID[5:] + customerID
	trackNumber = entry + strings.ToUpper(trackNumber)

	delivery := genDelivery()
	items := genItems(trackNumber)
	payment := genPayment(orderUID, items)

	return &models.Order{
		OrderUid:          orderUID,
		TrackNumber:       trackNumber,
		Entry:             entry,
		Delivery:          delivery,
		Payment:           payment,
		Items:             items,
		Locale:            "en",
		InternalSignature: "",
		CustomerID:        customerID,
		DeliveryService:   "delivery_light",
		ShardKey:          strconv.Itoa(rand2.Intn(10)),
		SMID:              rand2.Intn(100),
		DateCreated:       time.Now().Format(time.RFC3339),
		OOFShard:          strconv.Itoa(rand2.Intn(100)),
	}
}

func genDelivery() models.Delivery {
	names := []string{"Test Testov", "George Bush", "Alexandr the Great", "Crazy Lunatic", "Moonlight walker"}
	regions := []string{"Africa", "America", "Europe", "Oceania"}
	cities := []string{"London", "Moscow", "Paris", "Madrid", "Milan"}
	addresses := []string{"Baker street 1", "Prospekt mira 12", "Flower street 55", "Monstadt street 35"}
	emails := []string{"test", "red", "blue", "white", "green"}

	return models.Delivery{
		Name:    names[rand2.Intn(len(names))],
		Phone:   genPhoneNumber(),
		Zip:     strconv.Itoa(rand2.Intn(1234567)),
		City:    cities[rand2.Intn(len(cities))],
		Address: addresses[rand2.Intn(len(addresses))],
		Region:  regions[rand2.Intn(len(regions))],
		Email:   emails[rand2.Intn(len(emails))] + "@wb.ru",
	}
}

func genItems(track string) []models.Item {
	var items []models.Item
	numItems := 1 + rand2.Intn(3)
	itemNames := []string{"Mascaras", "cool_name", "gel v1", "airforce", "prime"}
	brandNames := []string{"Nike", "Adidas", "Befree", "Reebok", "Asics"}

	for t := 0; t < numItems; t++ {
		rid, err := genHash(ridLen)
		if err != nil {
			log.Printf("failed to generate rid %#v", err)
		}

		price := 1 + rand2.Intn(500)
		sale := rand2.Intn(30)
		totalPrice := price - int(float64(price)*(float64(sale)/100))

		item := models.Item{
			ChrtId:      rand2.Intn(1000000),
			TrackNumber: track,
			Price:       price,
			Rid:         rid + customerID,
			Name:        itemNames[rand2.Intn(len(itemNames))],
			Sale:        sale,
			Size:        strconv.Itoa(rand2.Intn(10)),
			TotalPrice:  totalPrice,
			NmId:        rand2.Intn(1000000),
			Brand:       brandNames[rand2.Intn(len(brandNames))],
			Status:      202,
		}

		items = append(items, item)
	}

	return items
}

func genPayment(transaction string, items []models.Item) models.Payment {
	currencies := []string{"USD", "RUB", "EUR", "CNY", "JPY"}
	providers := []string{"wbpay", "sberpay", "mirpay", "cnpay"}
	banks := []string{"wb", "ozon", "tinkoff", "alpha", "rusfinance"}

	var goodsTotal int

	for i := 0; i < len(items); i++ {
		goodsTotal += items[i].TotalPrice
	}

	return models.Payment{
		Transaction:  transaction,
		RequestId:    "",
		Currency:     currencies[rand2.Intn(len(currencies))],
		Provider:     providers[rand2.Intn(len(providers))],
		Amount:       1 + rand2.Intn(5000),
		PaymentDt:    1 + rand2.Intn(1000000000),
		Bank:         banks[rand2.Intn(len(banks))],
		DeliveryCost: 1 + rand2.Intn(2000),
		GoodsTotal:   goodsTotal,
		CustomFee:    0,
	}
}

func genHash(length int) (string, error) {
	byteLen := (length + 1) / 2
	randomBytes := make([]byte, byteLen)

	if _, err := rand.Read(randomBytes); err != nil {
		return "", err
	}

	hashString := hex.EncodeToString(randomBytes)
	return hashString[:length], nil
}

func genString(length int) (string, error) {
	const alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	randomBytes := make([]byte, length)

	if _, err := rand.Read(randomBytes); err != nil {
		return "", err
	}

	for i := 0; i < length; i++ {
		randomBytes[i] = alphabet[int(randomBytes[i])%length]
	}

	return string(randomBytes), nil
}

func genPhoneNumber() string {
	rand2.Seed(time.Now().UnixNano())

	first := rand2.Intn(9) + 1
	areaCode := rand2.Intn(900) + 100
	firstPart := rand2.Intn(900) + 100
	secondPart := rand2.Intn(9000) + 1000

	phoneNumber := fmt.Sprintf("+%1d%03d%03d%04d", first, areaCode, firstPart, secondPart)

	return phoneNumber
}
