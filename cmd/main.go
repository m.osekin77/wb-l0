package main

import (
	"context"
	"errors"
	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
	"l0/config"
	"l0/internal/database/cache"
	"l0/internal/database/postgres"
	"l0/internal/datagen"
	"l0/internal/handlers"
	"l0/internal/utils"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

func main() {
	cfg, err := config.NewConfig()
	if err != nil {
		log.Fatal(err)
		return
	}
	log.Println("[+] read config file")

	no, err := utils.NewNats(&cfg.Nats)
	if err != nil {
		log.Fatal(err)
	}

	defer no.Nc.Close()

	conn, err := postgres.Connect(&cfg.Postgres)
	if err != nil {
		log.Fatalf("failed to connect to postgres %#v\n", err)
	}

	log.Println("[+] connected to postgres!")
	defer conn.Close()

	db := postgres.NewDB(conn)
	c := cache.NewCache(db)
	c.LoadFromDB()

	ch := handlers.NewCacheHandler(c)

	r := mux.NewRouter()
	r.HandleFunc("/", ch.GetOrderData)

	server := &http.Server{
		Addr:    ":" + cfg.HTTP.Port,
		Handler: r,
	}

	validate := validator.New(validator.WithRequiredStructEnabled())

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)

	ctx, cancel := context.WithCancel(context.Background())
	wg := &sync.WaitGroup{}
	wg.Add(1)

	go func() {
		defer wg.Done()
		for {
			select {
			case <-ctx.Done():
				log.Println("[+] producer was stopped")
				return
			default:
				order := datagen.GenOrder()
				err = validate.Struct(*order)
				if err != nil {
					if _, ok := err.(*validator.InvalidValidationError); ok {
						log.Println(err)
						return
					}

					for _, verr := range err.(validator.ValidationErrors) {
						log.Printf("Field: %s, Tag: %s, Actual: %v\n", verr.Field(), verr.Tag(), verr.Value())
					}
					return
				}
				err = no.Publish(order)
				log.Println("[+] order published !")
				if err != nil {
					log.Printf("failed to publish message %#v", err)
				}
				time.Sleep(2 * time.Second)
			}
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case <-ctx.Done():
				log.Println("[+] consumer was stopped")
				return
			default:
				order, err := no.Subscribe(ctx)
				if err != nil {
					log.Printf("failed to recieve message %#v", err)
					continue
				}
				if order != nil {
					err = db.InsertOrder(order)
					if err != nil {
						log.Printf("failed to insert order in postgres %#v", err)
					} else {
						log.Printf("[+] inserted in database %#v\n", order.OrderUid)
						c.WriteCache(order)
					}
					time.Sleep(2 * time.Second)
				}
			}
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		log.Println("[+] starting the server")
		if err = server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Printf("server error: %#v", err)
			return
		}
	}()

	select {
	case <-shutdown:
		cancel()
		if err = server.Shutdown(ctx); err != nil {
			log.Println("failed to shut down the server")
		}
		log.Println("[+] server gracefully stopped")
	}

	wg.Wait()
	log.Println("[+] all goroutines finished")
}
